Bilean
======

--------
Overview
--------

Bilean is a billing service for OpenStack clouds, it provides trigger-type
billing based on other OpenStack services' notification.

---------
Resources
---------

Launchpad Projects
------------------
- Server: https://launchpad.net/bilean

Blueprints
----------
- Blueprints: https://blueprints.launchpad.net/bilean

Bug Tracking
------------
- Bugs: https://bugs.launchpad.net/bilean

Documentation
------------
- Documentation: http://bilean.readthedocs.io/en/latest

IRC
---
IRC Channel: #openstack-bilean on `Freenode`_.


.. _Freenode: http://freenode.net/
