======================
Contributing to Bilean
======================

If you're interested in contributing to the Bilean project,
the following will help get you started.

Contributor License Agreement
=============================

In order to contribute to the Bilean project, you need to have
signed OpenStack's contributor's agreement:

* http://docs.openstack.org/infra/manual/developers.html
* http://wiki.openstack.org/CLA


Project Hosting Details
=======================

* Bug trackers
    * General bilean tracker: https://launchpad.net/bilean

    * Python client tracker: https://launchpad.net/python-bileanclient

* Mailing list (prefix subjects with ``[Bilean]`` for faster responses)
    http://lists.openstack.org/cgi-bin/mailman/listinfo/openstack-dev

* Wiki
    https://wiki.openstack.org/wiki/Bilean

* IRC channel
    * #openstack-bilean at FreeNode

* Code Hosting
    * https://git.openstack.org/cgit/openstack/bilean

    * https://git.openstack.org/cgit/openstack/python-bileanclient

* Code Review
    * https://review.openstack.org/#/q/bilean+AND+status:+open,n,z

    * http://docs.openstack.org/infra/manual/developers.html#development-workflow
