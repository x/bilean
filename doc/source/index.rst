..
  Licensed under the Apache License, Version 2.0 (the "License"); you may
  not use this file except in compliance with the License. You may obtain
  a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  License for the specific language governing permissions and limitations
  under the License.

====================================
Welcome to the Bilean documentation!
====================================

Bilean is a billing service for OpenStack cloud, it provides trigger-type
billing based on other OpenStack services' notification.

This documentation offers information on how Bilean works and how to
contribute to the project.

.. toctree::
   :maxdepth: 1

   overview
   install
   contributing

Indices and tables
~~~~~~~~~~~~~~~~~~

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
